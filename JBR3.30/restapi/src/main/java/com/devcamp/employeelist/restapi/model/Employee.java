package com.devcamp.employeelist.restapi.model;

public class Employee {
    int id;
    String firstName;
    String lastName;
    int salary;
    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }
    public Employee() {
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public int getAnualSalary() {
        return (this.salary)*12;
    }
    public int raiseSalary(int percent) {
        int newSalary = (this.salary)*(1 + percent/100);
        return newSalary;
    }
    public String getName() {
        return firstName + " " + lastName;
    }
    public String toString() {
        return "Employee[id = " + id + ", name = " + this.getName() + ", salary = " + salary + "]";
    }
}
