package com.devcamp.employeelist.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employeelist.restapi.model.Employee;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("employee")
    public ArrayList<Employee> employeeList() {
        Employee employee1 = new Employee(1, "John", "Doe", 5000);
        Employee employee2 = new Employee(2, "Mary", "Sidey", 4000);
        Employee employee3 = new Employee(3, "Joe", "Nguyen", 6000);
        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        System.out.println(employee3.toString());

        ArrayList<Employee> list = new ArrayList<>();
        list.add(employee1);
        list.add(employee2);
        list.add(employee3);
        return list;
    }
}
